module Api
  module V1
class ArchdiocesesController < ApplicationController
  before_action :set_archdiocese, only: [:show, :update, :destroy]

  # GET /archdiocese
  # GET /archdiocese.json
  def index
    @archdioceses = Archdiocese.all

    render json: @archdioceses
  end

  # GET /archdiocese/1
  # GET /archdiocese/1.json
  def show
    render json: @archdiocese
  end

  # POST /archdiocese
  # POST /archdiocese.json
  def create
    @archdiocese = Archdiocese.new(archdiocese_params)

    if @archdiocese.save
      render json: @archdiocese, status: :created, location: @archdiocese
    else
      render json: @archdiocese.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /archdiocese/1
  # PATCH/PUT /archdiocese/1.json
  def update
    @archdiocese = Archdiocese.find(params[:id])

    if @archdiocese.update(archdiocese_params)
      head :no_content
    else
      render json: @archdiocese.errors, status: :unprocessable_entity
    end
  end

  # DELETE /archdiocese/1
  # DELETE /archdiocese/1.json
  def destroy
    @archdiocese.destroy

    head :no_content
  end

  private

    def set_archdiocese
      @archdiocese = Archdiocese.find(params[:id])
    end

    def archdiocese_params
      params.require(:archdiocese).permit(:name)
    end
end
end
end

