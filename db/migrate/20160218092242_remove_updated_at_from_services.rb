class RemoveUpdatedAtFromServices < ActiveRecord::Migration
  def change
    remove_column :services, :updated_at, :string
  end
end
