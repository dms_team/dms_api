class RemoveCreatedAtFromParishes < ActiveRecord::Migration
  def change
    remove_column :parishes, :created_at, :string
  end
end
