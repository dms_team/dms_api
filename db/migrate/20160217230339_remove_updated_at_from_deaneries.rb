class RemoveUpdatedAtFromDeaneries < ActiveRecord::Migration
  def change
    remove_column :deaneries, :updated_at, :string
  end
end
