class RemoveCreatedAtFromMembers < ActiveRecord::Migration
  def change
    remove_column :members, :created_at, :string
  end
end
