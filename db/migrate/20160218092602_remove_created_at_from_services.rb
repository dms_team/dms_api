class RemoveCreatedAtFromServices < ActiveRecord::Migration
  def change
    remove_column :services, :created_at, :string
  end
end
