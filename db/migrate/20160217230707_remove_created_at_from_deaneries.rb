class RemoveCreatedAtFromDeaneries < ActiveRecord::Migration
  def change
    remove_column :deaneries, :created_at, :string
  end
end
