class AddNameToDeaneries < ActiveRecord::Migration
  def change
    add_column :deaneries, :name, :string
  end
end
