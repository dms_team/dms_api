class RemoveCreatedAtFromArchdiocese < ActiveRecord::Migration
  def change
    remove_column :archdioceses, :created_at, :string
  end
end
