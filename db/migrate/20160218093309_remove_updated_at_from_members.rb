class RemoveUpdatedAtFromMembers < ActiveRecord::Migration
  def change
    remove_column :members, :updated_at, :string
  end
end
