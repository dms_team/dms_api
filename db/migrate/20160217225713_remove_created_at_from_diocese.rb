class RemoveCreatedAtFromDiocese < ActiveRecord::Migration
  def change
    remove_column :dioceses, :created_at, :string
  end
end
