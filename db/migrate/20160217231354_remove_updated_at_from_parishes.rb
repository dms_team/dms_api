class RemoveUpdatedAtFromParishes < ActiveRecord::Migration
  def change
    remove_column :parishes, :updated_at, :string
  end
end
