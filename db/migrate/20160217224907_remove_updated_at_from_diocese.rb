class RemoveUpdatedAtFromDiocese < ActiveRecord::Migration
  def change
    remove_column :dioceses, :updated_at, :string
  end
end
