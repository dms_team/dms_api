class RemoveUpdatedAtFromArchdiocese < ActiveRecord::Migration
  def change
    remove_column :archdioceses, :updated_at, :string
  end
end
